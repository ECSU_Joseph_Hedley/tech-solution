**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Tech Solutions

None of our team actually knows how to code. We included the voter registration tool for Rock the Vote, which we intend to modify.

1. By incentivizing voter registration for both unregistered voters and high school students we will increase voter registration and participation
2. Users will accumulate points for number of voters registered
3. HBCUs in partnership with local high schools can provide a hub for resources and prizes
4. Virtual prizes may be redeemable immediately
